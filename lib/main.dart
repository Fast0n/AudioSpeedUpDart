import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:google_fonts/google_fonts.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Audio Speed Up',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Color(0xFF014E17),
        accentColor: Color(0xFF488E48),
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'Audio Speed Up'),
    );
  }
}

// non toccare
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

// inserire tutte le veriabili e funzioni da usare in tutto il codice
class _MyHomePageState extends State<MyHomePage> {
  int _value = 0;
  double _valuedeafult = 2.0;

  _launchURL(url) async {
    // Check if can launch URL
    if (await canLaunch(url))
      await launch(url);
    else
      throw 'Could not launch $url';
  }

  void _showDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Come si usa:'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Seleziona un file da riprodurre usando l\'opzione condividi da app come WhatsApp.\n\nATTENZIONE: Alcuni formati audio sono supportati solo nelle ultime versioni Android.'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final menuButton = new PopupMenuButton<int>(
      onSelected: (int i) {},
      itemBuilder: (BuildContext ctx) {},
      child: new Icon(
        Icons.more_vert,
      ),
    );

    // Scaffold per aiutarci a vedere meglio il codice
    return Scaffold(
      appBar: AppBar(
        title: new Center(
            child: new Text(widget.title, textAlign: TextAlign.center)),
        actions: [
          menuButton,
        ],
      ),
      body: Stack(
        children: <Widget>[
          SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                      new Container(
                        margin: new EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                        child: Text(
                          'Scegli la velocità',
                          style: TextStyle(
                              fontSize: 30.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                      new Container(
                        margin: new EdgeInsets.fromLTRB(7.0, 0.0, 7.0, 0.0),
                        child: Card(
                          child: Column(
                            children: <Widget>[
                              new Container(
                                margin:
                                    new EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
                                child: Column(children: <Widget>[
                                  Text(
                                    'Velocità: ${_valuedeafult.toDouble()}x',
                                    style: TextStyle(fontSize: 18.0),
                                  ),
                                  Slider(
                                    activeColor: Color(0xFF488E48),
                                    inactiveColor: Color(0xFF488E48),
                                    value: _valuedeafult,
                                    min: 0.5,
                                    max: 2,
                                    divisions: 6,
                                    onChanged: (double value) {
                                      setState(() {
                                        _valuedeafult = value;
                                      });
                                    },
                                  )
                                ]),
                              )
                            ],
                          ),
                        ),
                      ),
                    ])),
                new Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      new Container(
                        margin: new EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                        child: Text(
                          'Audio di prova',
                          style: TextStyle(
                              fontSize: 30.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                      new Container(
                        margin: new EdgeInsets.fromLTRB(7.0, 0.0, 7.0, 35.0),
                        child: Card(
                          child: Column(
                            children: <Widget>[
                              new Container(
                                  margin: new EdgeInsets.fromLTRB(
                                      5.0, 0.0, 5.0, 0.0),
                                  child: Slider(
                                    value: _value.toDouble(),
                                    min: 0,
                                    max: 100,
                                    divisions: 100,
                                    activeColor: Color(0xFF488E48),
                                    inactiveColor: Color(0xFF488E48),
                                    onChanged: (double newValue) {
                                      setState(() {
                                        _value = newValue.round();
                                      });
                                    },
                                  )),
                              new Container(
                                  margin: new EdgeInsets.fromLTRB(
                                      0.0, 5.0, .0, 5.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      MaterialButton(
                                          minWidth: 40,
                                          height: 40,
                                          textColor: Color(0xFF488E48),
                                          onPressed: null,
                                          child:
                                              (Icon(Icons.refresh, size: 40))),
                                      MaterialButton(
                                          minWidth: 40,
                                          height: 40,
                                          textColor: Color(0xFF488E48),
                                          onPressed: null,
                                          child: (Icon(Icons.play_arrow,
                                              size: 40))),
                                      MaterialButton(
                                          onPressed: null,
                                          minWidth: 40,
                                          height: 40,
                                          textColor: Color(0xFF488E48),
                                          child: (Icon(Icons.stop, size: 40))),
                                    ],
                                  )),
                              new Container(
                                  margin: new EdgeInsets.fromLTRB(
                                      0.0, 5.0, .0, 5.0),
                                  child: OutlineButton(
                                      borderSide: BorderSide(
                                          color: Color(0xFF488E48), width: 1.0),
                                      textColor: Color(0xFF488E48),
                                      highlightedBorderColor: Color(0xFF488E48),
                                      onPressed: () {
                                        // _incrementCounter();
                                      },
                                      child: (Text("Test",
                                          style: TextStyle(
                                              fontFamily: 'Roboto',
                                              fontSize: 20.0))))),
                            ],
                          ),
                        ),
                      ),
                      new Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            RaisedButton(
                              textColor: Colors.black54,
                              color: Color(0xFF488E48),
                              splashColor: Color(0xFF488E48),
                              onPressed: () {
                                _showDialog();
                              },
                              child: Text(
                                "Aiuto",
                                style: TextStyle(
                                    fontSize: 20.0, fontFamily: 'Roboto'),
                              ),
                            ),
                            Spacer(),
                            OutlineButton(
                                borderSide: BorderSide(
                                    color: Color(0xFF488E48), width: 1.0),
                                textColor: Color(0xFF488E48),
                                highlightedBorderColor: Color(0xFF488E48),
                                onPressed: () {
                                  _launchURL(
                                      "https://github.com/FastRadeox/MensaUniurbBot");
                                },
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      child: SvgPicture.asset(
                                        'assets/paypal.svg',
                                        color: Color(0xFF488E48),
                                        width: 20,
                                        height: 20,
                                      ),
                                      margin: EdgeInsets.only(right: 7),
                                    ),
                                    Text("Dona",
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            fontFamily: 'Roboto'))
                                  ],
                                ))
                          ],
                        ),
                        margin: new EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 5.0),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
